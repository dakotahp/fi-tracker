blue="0;34"
green="0;32"
red="0;31"
purple="0;35"

case Rails.env
when "production"
  important_message = "\001\e[#{red}m\002YOU ARE IN #{Rails.env.upcase}! DON'T FUCK THIS UP!"
when "staging"
  important_message = "\001\e[#{blue}m\002YOU ARE IN #{Rails.env.upcase}! Be gentle!"
end

Pry.prompt = [
  proc { |target_self, nest_level, pry|
    "[#{pry.input_array.size}]\001\e[0;32m\002#{Pry.config.prompt_name}\001\e[0m\002(\001\e[0;33m\002#{Pry.view_clip(target_self)}\001\e[0m\002) #{important_message}#{":#{nest_level}" unless nest_level.zero?}> "
  },
  proc { |target_self, nest_level, pry|
    "[#{pry.input_array.size}]\001\e[1;32m\002#{Pry.config.prompt_name}\001\e[0m\002(\001\e[1;33m\002#{Pry.view_clip(target_self)}\001\e[0m\002) #{important_message}#{":#{nest_level}" unless nest_level.zero?}* "
  }
]
