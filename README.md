# FI Tracker

[![build status](https://gitlab.com/dakotahp/fi-tracker/badges/master/build.svg)](https://gitlab.com/dakotahp/fi-tracker/commits/master)

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Versions
* Ruby 2.4.1
* Rails 5.1.4

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

## Setup

### Start Gitlab Runner

To run continuous integration jobs to run tests on Gitlab, `gitlab-runner` needs to be
installed.

```
brew install gitlab-runner
```

If the jobs seem to be stuck in the [runners page](https://gitlab.com/dakotahp/fi-tracker/settings/ci_cd) then start them with:

```
gitlab-runner start [job-name]
```

### Postgres Gem Compilation

If there are issues installing the `pg` gem you can use the Postgres.app build stuff with

```
gem install pg -- --with-pg-config=/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_config
```

## Technical Features
- Background jobs (using Active Job and sidekiq)
- Decorators
- Façades
- Charts


## To Do
- Set up sidekiq and redis
- Create preferences with gross monthly income per person,
email notifications (monthly_reminder_time, monthly_reminder_enabled)
- Set up background job to send monthly notification
-
