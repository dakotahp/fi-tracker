import BalanceManager from "../managers/balance_manager"
import TextField from "./text_field"
import DateField from "./date_field"
import moment from "moment"
import BalanceFormManager from "../managers/balance_form_manager"
import React from "react"

const BalanceCreate = React.createClass({
  propTypes: {
    default_required_fields: React.PropTypes.array,
    api_auth_token: React.PropTypes.string,
    balances_resource_url: React.PropTypes.string,
    index_manager: React.PropTypes.object,
  },

  getDefaultProps: function() {
    return {
      default_required_fields: ["amount"],
    }
  },

  componentWillMount: function() {
    this.manager = new BalanceManager({
      component: this,
      index_manager: this.props.index_manager
    });

    this.form_manager = new BalanceFormManager({
      component: this,
      required_fields: this.props.default_required_fields,
    });
  },

  getInitialState: function() {
    return {
      balance: {
        amount: 0,
        recorded_on: moment.utc(),
      }
    };
  },

  handleDelete: function(event) {
    event.preventDefault();

    this.balance_manager.deleteBalance(this.props.balance.id);
  },

  render: function() {
    var that = this;

    return (
      <section>
        <div className="field">
          <TextField label="Balance"
            name="amount"
            param="amount"
            initial_value={this.state.balance.amount.toString()}
            onChange={this.form_manager.fieldChange}
            type="number" />
        </div>
        <div className="field">
          <DateField label="Recorded On"
            name="Recorded On"
            param="recorded_on"
            initial_value={this.state.balance.recorded_on}
            onChange={this.form_manager.fieldChange} />
        </div>

        <div className="actions">
          <input type="submit" value="Update" className="button" onClick={this.form_manager.submit} />
        </div>
      </section>
    )
  }
});

export default BalanceCreate;
