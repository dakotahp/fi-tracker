import BalanceShow from "./balance_show"
import BalanceCreate from "./balance_create"
import BalanceIndexManager from "../managers/balance_index_manager"
import TrackerClient from "../tracker_client"
import _ from "underscore"
import React from "react"

const BalanceIndex = React.createClass({
  propTypes: {
    api_auth_token: React.PropTypes.string,
    balances_resource_url: React.PropTypes.string,
    default_required_fields: React.PropTypes.array,
    financial_account_resource_url: React.PropTypes.string,
    financial_account: React.PropTypes.object,
  },

  getInitialState: function() {
    return {balances: []};
  },

  componentWillMount: function() {
    this.manager = new BalanceIndexManager({
      component: this
    });
  },

  componentDidMount: function() {
    var that = this;

    var tracker_client = new TrackerClient({
      api_auth_token: this.props.api_auth_token
    });

    tracker_client.sendRequest({
      method: "GET",
      url: this.props.financial_account_resource_url
    }).done(function(data) {
      var balances = _.sortBy(data.included, function(balance) {
        return -new Date(balance.attributes.recorded_on);
      });

      that.setState({balances: balances});
    });
  },

  toggle_form: function(event) {
    event.preventDefault();
    $('.balance_create_form').slideToggle();
  },

  render: function() {
    var that = this,
        balances;

    var balances = _.sortBy(this.state.balances, function(balance) {
      return -new Date(balance.attributes.recorded_on);
    });

    if (balances) {
      var cards = balances.map(function(balance, index) {
        var url = document.links.self;

        return (
          <BalanceShow
            api_auth_token={that.props.api_auth_token}
            index_manager={that.manager}
            balance={balance}
            key={balance.id}
            resource_url={url} />
        )
      });

      return (
        <section>
          <div className="row spacer-bottom-5">
            <div className="columns small-6">
              <h4 className="bold">Balance History</h4>
            </div>
            <div className="columns small-6 text-right">
              <a href="#toggle-form" className="button spacer-bottom-0" onClick={this.toggle_form}>
                Update Balance
              </a>
            </div>
          </div>

          <div className="hidden balance_create_form">
            <BalanceCreate
              api_auth_token={this.props.api_auth_token}
              balances_resource_url={this.props.balances_resource_url}
              financial_account={this.props.financial_account}
              index_manager={that.manager} />
          </div>

          <table>
            <thead>
              <tr>
                <th>Balance</th>
                <th>Recorded On</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              { cards }
            </tbody>
          </table>
        </section>
      )
    } else {
      return (
        <p>loading…</p>
      )
    }
  }
});

export default BalanceIndex;
