import BalanceIndex from "./balance_index"
import BalanceManager from "../managers/balance_manager"
import React from "react"

const BalanceShow = React.createClass({
  propTypes: {
    balance: React.PropTypes.object.isRequired,
    index_manager: React.PropTypes.object.isRequired,
  },

  componentDidMount: function() {
    this.balance_manager = new BalanceManager({
      component: this,
      index_manager: this.props.index_manager
    });
  },

  getInitialState: function() {
    return {};
  },

  handleDelete: function(event) {
    event.preventDefault();

    if(confirm('Are you sure?')) {
      this.balance_manager.deleteBalance(this.props.balance.id);
    }
  },

  render: function() {
    var that = this,
        balance = this.props.balance.attributes;

    return (
      <tr>
        <td><a href={this.props.balance.links.self_edit}>{ balance.amount }</a></td>
        <td>{ balance.created }</td>
        <td>
          <a href="#delete" onClick={this.handleDelete}>Delete</a>
        </td>
      </tr>
    )
  }
});

export default BalanceShow;
