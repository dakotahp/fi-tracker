import _ from "underscore"
import leftpad from "left-pad"
import React from "react"

const DateField = React.createClass({
  propTypes: {
    initial_value: React.PropTypes.object,
    name: React.PropTypes.string,
    onChange: React.PropTypes.func,
    param: React.PropTypes.string
  },

  getInitialState: function() {
    var month = "",
        day = "",
        year = "",
        initial_date;

    if(this.props.initial_value) {
      month = this.props.initial_value.format("MM");
      day = this.props.initial_value.format("DD");
      year = this.props.initial_value.format("YYYY");
    }

    return {day: day, month: month, year: year};
  },

  handleSelectChange: function(event) {
    var key = event.target.id,
        value = event.target.value,
        object = {};

    object[key] = value;

    this.setState(object, function(){
      this.sendFormattedDate();
    });
  },

  formattedDate: function() {
    var day = this.state.day,
        month = this.state.month,
        year = this.state.year;

    if (day.length && month.length && year.length) {
      var date = new Date(year + "-" + month + "-" + day + "T00:00:00");
    }

    return date;
  },

  sendFormattedDate: function() {
    var date = this.formattedDate();
    this.props.onChange({param: this.props.param, value: date});
  },

  render: function() {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
      "Sep", "Oct", "Nov", "Dec"],
        days = _.range(1, 32),
        daysPadded = days.map(function (n) { return leftpad(n, 2, 0) }),
        years = _.range(1980, 2031);

    return (
      <div className="input date">
        <label>{this.props.name}</label>

        <select id="month" onChange={this.handleSelectChange} value={this.state.month}>
          <option value="">Month</option>
          { months.map(function(month, index) {
            var integer = index + 1,
                value = leftpad(integer, 2, 0);
            return <option key={index} value={value}>{month}</option>
          })}
        </select>

        <select id="day" onChange={this.handleSelectChange} value={this.state.day}>
          <option value="">Day</option>
          { daysPadded.map(function(value, index) {
            return <option key={index} value={value}>{days[index]}</option>
          })}
        </select>

        <select id="year" onChange={this.handleSelectChange} value={this.state.year}>
          <option value="">Year</option>
          { years.map(function(value, index) {
            return <option key={index} value={value}>{value}</option>
          })}
        </select>
      </div>
    );
  }
});

export default DateField;
