import React from "react"

const TextField = React.createClass({
  propTypes: {
    data_list_name: React.PropTypes.string,
    id: React.PropTypes.string,
    initial_value: React.PropTypes.string,
    label: React.PropTypes.string,
    onChange: React.PropTypes.func,
    param: React.PropTypes.string,
    type: React.PropTypes.string
  },

  getInitialState: function() {
    return {value: ""};
  },

  componentWillReceiveProps: function(next_props) {
    this.setState({value: next_props.initial_value});
  },

  handleInputChange: function(event) {
    this.setState({value: event.target.value}, function(){
      this.props.onChange({param: this.props.param, value: this.state.value});
    });
  },

  render: function() {
    var type = this.props.type || "text";

    return (
      <div className="input text spacer-bottom-4">
        <label>{this.props.label}</label>
        <input
          className="string optional"
          type={type}
          value={this.state.value}
          onChange={this.handleInputChange} />
      </div>
    );
  }
});

export default TextField;
