import _ from "underscore"

const BalanceFormManager = function(options) {
  const that = this;
  this.component = options.component;
  this.balance_manager = this.component.manager;
  this.required_fields = options.required_fields;

  this.fieldChange = function(options) {
    var new_state = that.component.state.balance;
    new_state[options.param] = options.value;
    that.component.setState({balance: new_state});
  };

  this.isValid = function() {
    return _.every(that.required_fields, function(field) {
      return that.component.state.balance[field]
    });
  };

  this.submit = function(event) {
    event.preventDefault();

    if (!that.isValid()) {
      that.balance_manager.component.setState({
        error: "Please complete all fields and upload attachments"
      });

      return false;
    }

    // that.balance_manager.component.setState({ error: null });
    // that.component.setState({ submitting: true });
    var attributes = JSON.parse(JSON.stringify(that.component.state.balance));
    attributes['financial_account_uuid'] = that.balance_manager.component.props.financial_account.uuid;

    var request_data = {
      attributes,
      financial_account_uuid: that.balance_manager.component.props.financial_account.uuid,
    };

    that.balance_manager.createBalance(
      attributes,
      function(data) {
        console.log("test")
        var component = that.component.manager.component;
        component.setState({
          metadata: that.component.state.document,
        });
      }
    );
  };
};

module.exports = BalanceFormManager;
