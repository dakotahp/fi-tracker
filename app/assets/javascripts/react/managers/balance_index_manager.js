import update from "immutability-helper"

const BalanceIndexManager = function(options) {
  const that = this;

  this.component = options.component;

  this.handleDocumentCreateError = function(data) {
    console.error(data);
  };

  this.handleBalanceCreate = function(balance) {
    let new_state = update(
      this.component.state, {
      balances: {$push: [balance.data]}
    });

    this.component.setState(new_state);
  };

  this.deleteBalance = function(balance_id) {
    var that = this,
        data = {
          data: {
            id: balance_id,
            type: "balances"
          }
        };

      return this.tracker_client.sendRequest({
        data: JSON.stringify(data),
        method: "DELETE",
        url: this.component.props.resource_url
      }).done(function() {
        that.handleBalanceDelete(that.component.state.document);
      }).fail(function(data) {
        that.handleDocumentDeleteError(data);
      });
  };

  this.handleBalanceDelete = function(balance) {
    var component = that.component.props.index_manager.component,
        index = component.state.documents.map(d => d.id).indexOf(document.data.id);

    component.setState({
      documents: update(
        component.state.balances,
        {$splice: [[index, 1]]
      })
    });
  };
};

module.exports = BalanceIndexManager;
