import TrackerClient from "../tracker_client"
import update from "immutability-helper"

const BalanceManager = function(options) {
  const that = this;
  this.component = options.component;
  this.index_manager = options.index_manager;

  this.tracker_client = new TrackerClient({
    api_auth_token: this.component.props.api_auth_token
  });

  this.init = function() {
  };

  this.createBalance = function(attributes, callback) {
    var that = this,
        data = {
          data: {
            type: "balances",
            attributes: attributes
          }
        };

    var url = this.component.props.balances_resource_url;

    this.tracker_client.sendRequest({
      data: JSON.stringify(data),
      method: "POST",
      url: url
    }).done(function(data) {
      that.index_manager.handleBalanceCreate(data);
    });
  };

  this.deleteBalance = function(balance_id) {
    var that = this;

    return this.tracker_client.sendRequest({
      method: "DELETE",
      url: this.component.props.balance.links.self
    }).done(function() {
      that.handleBalanceDelete(that.component.props.balance);
    }).fail(function(data) {
      that.handleBalanceDeleteError(data);
    });
  };

  this.handleBalanceDelete = function(balance) {
    var component = that.component.props.index_manager.component,
        index = component.state.balances.map(d => d.id).indexOf(balance.id);

    component.setState({
      balances: update(
        component.state.balances,
        {$splice: [[index, 1]]
      })
    });
  };

  this.handleBalanceDeleteError = function(data) {
    console.error("Error deleting balance", data);
  };
};

module.exports = BalanceManager;
