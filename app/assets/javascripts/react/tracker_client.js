import _ from "underscore";

const TrackerClient = function(options) {
  const that = this;
  this.api_auth_token = options.api_auth_token;

  this.default_options = {
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Content-Type", "application/vnd.api+json");
      xhr.setRequestHeader("Authorization", that.api_auth_token);
    }
  };

  this.sendRequest = function(request) {
    return $.ajax(_.extend(request, that.default_options));
  };
};

module.exports = TrackerClient;
