// Global variables
window.React = require("react")
window.ReactDOM = require("react-dom")
window.Base64 = require("base-64")

// App components used in web views
window.BalanceIndex = require("./react/components/balance_index")
window.BalanceCreate = require("./react/components/balance_create")

// React-rails helpers
require("./react_rails/react_ujs_mount")
require("./react_rails/react_ujs_pjax")
require("./react_rails/react_ujs_native")
require("./react_rails/react_ujs_event_setup")
