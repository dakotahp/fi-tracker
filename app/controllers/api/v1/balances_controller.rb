class API::V1::BalancesController < API::V1::BaseController
  def context
    {
      current_user: current_user,
      financial_account: financial_account
    }
  end

  def financial_account
    FinancialAccount.find_by_uuid(params[:financial_account_id])
  end
end
