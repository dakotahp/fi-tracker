class API::V1::BaseController < ActionController::Base
  include JSONAPI::ActsAsResourceController

  protect_from_forgery with: :null_session

  before_action :destroy_session
  before_action :authenticate_user

  def context
    {
      current_user: current_user
     }
  end

  private

  def authenticate_user
    token, options = ActionController::HttpAuthentication::Token.token_and_options(request)

    user_email = options.blank?? nil : options[:email]
    user = user_email && User.find_by(email: user_email)

    if user && ActiveSupport::SecurityUtils.secure_compare(user.authentication_token, token)
      @current_user = user
    else
      deny_access
    end
  end

  def current_user
    @current_user
  end

  def data
    params[:data]
  end

  def deny_access
    render json: { message: "Unauthorized" }, status: 401
  end

  def destroy_session
    request.session_options[:skip] = true
  end
end
