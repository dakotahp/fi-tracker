class API::V1::SessionsController < API::V1::BaseController
  skip_before_action :authenticate_user, only: [:create]

  def create
    user = User.find_by(email: session_params[:email])

    if user && authenticate(session_params)
      user.generate_authentication_token!
      @current_user = user
      response = JSONAPI::ResourceSerializer.new(API::V1::SessionResource).
        serialize_to_hash(API::V1::SessionResource.new(user, nil))

      render(json: response, status: 201)
    else
      render json: { message: "Incorrect email/password" }, status: 401
    end
  end

  def destroy
    current_user.destroy_authentication_token!
    head :no_content
  end

  private

  def authenticate(params)
    Clearance.configuration.user_model.authenticate(
      params[:email], params[:password]
    )
  end

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
