class BalancesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_balance, only: [:show, :edit, :update, :destroy]

  def show
  end

  def edit
    @financial_account = @balance.financial_account
  end

  def create
    @financial_account = current_user.household.financial_accounts.find_by_uuid(
      params[:financial_account_id]
    )

    @balance = @financial_account.balances.create(balance_params)

    respond_to do |format|
      if @balance.save
        format.html { redirect_to financial_account_path(@balance.financial_account), notice: 'Balance was successfully created.' }
        format.json { render :show, status: :created, location: @balance }
      else
        format.html { render :new }
        format.json { render json: @balance.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @financial_account = FinancialAccount.find_by_uuid(params[:financial_account_id])

    respond_to do |format|
      format.json { render json: @financial_account.balances.order(:recorded_on) }
    end
  end

  def update
    respond_to do |format|
      if @balance.update(balance_params)
        format.html { redirect_to financial_account_path(@balance.financial_account), notice: 'Balance was successfully updated.' }
        format.json { render :show, status: :ok, location: @balance }
      else
        format.html { render :edit }
        format.json { render json: @balance.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @balance.destroy
    respond_to do |format|
      format.html { redirect_to balances_url, notice: 'Balance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_balance
      @balance = Balance.find_by_uuid(params[:id])
    end

    def balance_params
      params.require(:balance).permit(:amount, :recorded_on)
    end
end
