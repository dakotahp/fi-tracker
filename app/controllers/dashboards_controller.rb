class DashboardsController < ApplicationController
  before_action :authenticate_user!

  def index
    @financial_account = FinancialAccountDecorator.new(FinancialAccount.new)
    @financial_account.balances.build

    @household_net_worths = HouseholdNetWorthDecorator.decorate(
      HouseholdNetWorths.new(current_household)
    )

    @net_worth = HouseholdNetWorthDecorator.decorate(
      NetWorth.new(current_household)
    )

    @current_household = HouseholdDecorator.new(current_household)

    render 'pages/dashboard'
  end
end
