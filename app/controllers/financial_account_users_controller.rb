class FinancialAccountUsersController < ApplicationController
  before_action :authenticate_user!
  #before_action :set_financial_account_user, only: [:show, :edit, :update, :destroy]

  def create
    # @financial_account_user = FinancialAccountUser.create(
    #   financial_account:
    # )

    respond_to do |format|
      if @financial_account_user.save
        format.html { redirect_to @financial_account, notice: 'User successfully added to account.' }
      else
        format.html { redirect_to :back }
      end
    end
  end

  def destroy
  end

  private
    def financial_account_user_params
      params.require(:financial_account_user).permit(:user_id)
    end
end
