class FinancialAccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_financial_account, only: [:show, :edit, :update, :destroy]
  attr_reader :financial_account

  def index
    @financial_accounts = FinancialAccountDecorator.decorate_collection(
      current_household.financial_accounts
    )
    @household_net_worths = HouseholdNetWorthDecorator.decorate(
      HouseholdNetWorths.new(current_household)
    )

    @net_worth = NetWorth.new(current_household)
  end

  def show
    @balance = Balance.new
  end

  def new
    @financial_account = FinancialAccountDecorator.new(FinancialAccount.new)
    @financial_account.balances.build
  end

  def edit
  end

  def create
    @financial_account = current_user.household.financial_accounts.create(
      financial_account_params
    )

    balance = @financial_account.balance
    balance.recorded_on = Date.current

    respond_to do |format|
      if @financial_account.persisted? && balance.save
        format.html { redirect_to @financial_account, notice: 'Financial account was successfully created.' }
        format.json { render :show, status: :created, location: @financial_account }
      else
        format.html { render :new }
        format.json { render json: @financial_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @financial_account.update(financial_account_params)
        format.html { redirect_to @financial_account, notice: 'Financial account was successfully updated.' }
        format.json { render :show, status: :ok, location: @financial_account }
      else
        format.html { render :edit }
        format.json { render json: @financial_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @financial_account.destroy
    respond_to do |format|
      format.html { redirect_to financial_accounts_url, notice: 'Financial account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_financial_account
      @financial_account = FinancialAccountDecorator.decorate(
        current_household.financial_accounts.find_by_uuid(params[:id])
      )
    end

    def financial_account_params
      params.require(:financial_account).permit(
        :name,
        :subtype,
        balances_attributes: [
          :amount
        ]
      )
    end
end
