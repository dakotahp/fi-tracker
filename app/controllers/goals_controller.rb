class GoalsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_goal, only: [:show, :edit, :update, :destroy]

  def update
    respond_to do |format|
      if @goal.update(goal_params)
        format.html { redirect_to settings_path, notice: 'Goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @goal }
      else
        format.html { render :edit }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_goal
      @goal = current_user.household.goal
    end

    def goal_params
      params.require(:goal).permit(:net_worth, :passive_income)
    end
end
