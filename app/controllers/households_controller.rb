class HouseholdsController < ApplicationController
  def update
    respond_to do |format|
      if current_household.update(household_params)
        format.html { redirect_to settings_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: current_household }
      else
        format.html { redirect_to :back }
        format.json { render json: current_household.errors, status: :unprocessable_entity }
      end
    end
  end


  private

    def household_params
      params.require(:household).permit(:annual_income)
    end
end
