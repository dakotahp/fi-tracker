class IncomeSourcesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_income_source, only: [:show, :edit, :update, :destroy]

  def index
    @income_sources = IncomeSourceDecorator.decorate_collection(
      current_user.household.income_sources.alpha
    )
    @household_income_sources = HouseholdIncomeSourceDecorator.decorate(
      HouseholdIncomeSources.new(current_user.household)
    )
  end

  def new
    @income_source = IncomeSource.new
  end

  def edit
  end

  def create
    @income_source = current_user.household.income_sources.new(income_source_params)

    respond_to do |format|
      if @income_source.save
        format.html { redirect_to income_sources_path, notice: 'Income source was successfully created.' }
        format.json { render :show, status: :created, location: @income_source }
      else
        format.html { render :new }
        format.json { render json: @income_source.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @income_source.update(income_source_params)
        format.html { redirect_to income_sources_path, notice: 'Income source was successfully updated.' }
        format.json { render :show, status: :ok, location: income_sources_path }
      else
        format.html { render :edit }
        format.json { render json: @income_source.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @income_source.destroy
    respond_to do |format|
      format.html { redirect_to income_sources_url, notice: 'Income source was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_income_source
      @income_source = current_user.household.income_sources.find(params[:id])
    end

    def income_source_params
      params.require(:income_source).permit(:name, :monthly_amount)
    end
end
