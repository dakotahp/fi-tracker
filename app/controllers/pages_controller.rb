class PagesController < ApplicationController
  include HighVoltage::StaticPage
  layout :layout_for_page

  def show
    if current_page == 'pages/privacy'
      @privacy = render_markdown("#{Rails.root}/app/views/legal/privacy.md")
    elsif current_page == 'pages/terms'
      @terms = render_markdown("#{Rails.root}/app/views/legal/terms.md")
    end

    render template: current_page
  end

  private

    def layout_for_page
      if params[:id] == 'dashboard'
        'application'
      else
        'marketing'
      end
    end
end
