class SettingsController < ApplicationController
  before_action :authenticate_user!

  def index
    @goal = current_user.household.goal
    @user_setting = current_user.user_setting
    @user_invite = UserInvite.new
    @user_invites = current_user.household.user_invites.invited
  end
end
