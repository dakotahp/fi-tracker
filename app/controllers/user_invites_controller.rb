class UserInvitesController < ApplicationController
  before_action :set_user_invite, only: [:destroy]

  def create
    @user_invite = UserInvite.new(user_invite_params)
    link_user_to_invite

    respond_to do |format|
      if user_invite.save
        UserInviteMailer.invite(user_invite).deliver_later
        format.html { redirect_to settings_path, notice: 'User invite was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    user_invite.destroy

    redirect_to settings_path, notice: 'User invite was successfully deleted.'
  end

  def resend
    user_invite = find_user_invite
    UserInviteMailer.invite(user_invite).deliver_later

    redirect_to settings_path, notice: 'User invite was resent.'
  end

  private
    attr_reader :user_invite

    def find_user_invite
      current_user.user_invites.find_by_uuid(params[:id])
    end

    def link_user_to_invite
      @user_invite.user = current_user
      @user_invite.household = current_user.household
    end

    def set_user_invite
      @user_invite = current_user.user_invites.find_by_uuid(params[:id])
    end

    def user_invite_params
      params.require(:user_invite).permit(:email)
    end
end
