class UserSettingsController < ApplicationController
  before_action :set_user_setting, only: [:update]

  def update
    respond_to do |format|
      if @user_setting.update(user_setting_params)
        format.html { redirect_to settings_path, notice: 'User setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_setting }
      else
        format.html { render :edit }
        format.json { render json: @user_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_user_setting
      @user_setting = current_user.user_setting
    end

    def user_setting_params
      params.require(:user_setting).permit(
        :monthly_notification_enabled,
        :notify_monthly_at,
      )
    end
end
