class AssetDecorator < FinancialAccountDecorator
  def initialize(financial_account)
    super(financial_account)
  end
end
