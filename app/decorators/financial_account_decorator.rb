class FinancialAccountDecorator < SimpleDelegator
  include Rails.application.routes.url_helpers

  alias :undecorated :__getobj__

  def initialize(financial_account)
    super(financial_account)
  end

  def self.decorate(financial_account)
    klass = "#{financial_account.type}Decorator".constantize
    klass.new(financial_account)
  end

  def self.decorate_collection(financial_accounts)
    financial_accounts.collect do |financial_account|
      self.decorate(financial_account)
    end
  end

  def balance_to_human(show_cents = false)
    return empty_balance if balance.blank?

    if show_cents
      ActionController::Base.helpers.humanized_money_with_symbol(
        balance.amount
      )
    else
      ActionController::Base.helpers.money_without_cents_and_with_symbol(
        balance.amount
      )
    end
  end

  def balance_update_notice
    if updated_balance_needed?
      "(Updated balance needed)"
    end
  end

  def form_path
    if persisted?
      financial_account_path(self)
    else
      financial_accounts_path
    end
  end

  def last_updated
    difference = (Date.current - balance.recorded_on).to_i

    if difference > 1
      "#{difference} days ago"
    elsif difference == 0
      "Today"
    else
      "#{difference} day ago"
    end
  end

  def subtype_to_human
    I18n.t("financial_accounts.#{subtype}.name")
  end

  def has_current_balance_css_class
    updated_balance_needed? ? 'needs-update' : ''
  end

  def users_to_add(user)
    # users in household minus current_user and users already attached to account
    user.household.users.where.not(id: users)
  end

  def users_to_add_for_select(user)
    users_to_add(user).collect {|u| [ u.first_name, u.id ] }
  end

  def notated_balance
    balance_to_human
  end

  private

  def currency_symbol
    ActionController::Base.helpers.currency_symbol
  end

  def empty_balance
    "#{currency_symbol}0"
  end
end
