class HouseholdDecorator < SimpleDelegator
  alias :undecorated :__getobj__

  def amount_in_cash
    active_financial_accounts.cash.map(&:balance_amount).sum
  end

  def amount_in_debt
    active_financial_accounts.liabilities.map(&:balance_amount).sum
  end

  def amount_in_retirement
    active_financial_accounts.retirement.map(&:balance_amount).sum
  end
end
