class HouseholdIncomeSourceDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def goal_to_human
    money_without_cents_and_with_symbol current_user.goal.passive_income
  end

  def progress_rounded
    goal_progress.round
  end

  def passive_income_to_human
    money_without_cents_and_with_symbol passive_income
  end
end
