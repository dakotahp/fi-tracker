class HouseholdNetWorthDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def average_increase_to_human
    money_without_cents_and_with_symbol average_increase
  end

  def goal_to_human
    money_without_cents_and_with_symbol current_user.goal.net_worth
  end

  def progress_rounded
    [goal_progress.round.to_i, 100].min
  end

  def net_worth_to_human
    money_without_cents_and_with_symbol net_worth
  end
end
