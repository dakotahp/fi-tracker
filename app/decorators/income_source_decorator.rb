class IncomeSourceDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def amount_to_human
    money_without_cents_and_with_symbol monthly_amount
  end
end
