class LiabilityDecorator < FinancialAccountDecorator
  include Rails.application.routes.url_helpers

  def initialize(financial_account)
    super(financial_account)
  end

  def notated_balance
    "(#{balance_to_human})"
  end
end
