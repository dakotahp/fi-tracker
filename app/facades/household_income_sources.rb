class HouseholdIncomeSources
  attr_reader :household
  def initialize(household)
    @household = household
  end

  def passive_income
    household.income_sources.map{|is| is.monthly_amount}.reduce(0, :+)
  end

  def goal_progress
    ((passive_income / household.goal.passive_income.amount) * 100).round
  end
end
