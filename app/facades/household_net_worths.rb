class HouseholdNetWorths
  attr_reader :household

  def initialize(household)
    @household = household
  end

  def net_worth
    household.financial_accounts.map{|fa| fa.balance.amount}.reduce(0, :+)
  end

  def goal_progress
    ((net_worth / household.goal.net_worth.amount) * 100).round
  end
end
