class NetWorth
  attr_reader :household

  def initialize(household)
    @household = household
  end

  def amounts
    @amounts ||= amount_cents.map{|a| a / 100}
  end

  def amount_cents
    @amount_cents ||= net_worths_for_chart.collect(&:amount_cents)
  end

  def average_increase
    return 0 if amount_cents.size == 1
    differences = []

    amount_cents.each_with_index do |number, i|
      next if i == 0
      differences << number - amount_cents[i - 1]
    end

    average(differences) / 100
  end

  def balances
    @balances ||= net_worths_for_chart.reverse
  end

  def months
    @months ||= net_worths_for_chart.collect(&:month)
  end

  def net_worths
    @net_worths ||= begin
      household.balances.
        select("date_trunc('month', recorded_on) as month, sum(amount_cents) as amount_cents").
        group('month').
        order('month')
    end
  end

  def net_worths_for_chart
    net_worths.last(6)
  end

  def savings_rate
    return 0 if average_increase == 0

    (average_increase / (household.monthly_income_cents / 100)) * 100
  end

  private

  def average(values)
    values.inject(:+).to_f / values.length
  end
end
