module ApplicationHelper
  def app_name
    "FI Tracker"
  end

  def current_household
    current_user&.household
  end

  def embedded_svg(filename, options = {})
    root = options[:root] || Rails.root.join("app", "assets", "images", "svg")
    file = File.read(File.join(root, "#{filename}.svg"))
    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css "svg"
    if options[:class].present?
      svg["class"] = options[:class]
    end
    raw doc.to_html
  end
end
