module FinancialAccountHelper
  def financial_accounts_for_select
    @financial_accounts_for_select ||= begin
      assets = FinancialAccount::ASSETS.sort.map {|financial_account|
        [I18n.t("financial_accounts.#{financial_account}.name"), financial_account]
      }

      liabilities = FinancialAccount::LIABILITIES.sort.map {|financial_account|
        [I18n.t("financial_accounts.#{financial_account}.name"), financial_account]
      }

      [['Assets', assets], ['Liabilities', liabilities]]
    end
  end
end
