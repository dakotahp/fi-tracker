module ReactHelper
  def react_component(name, props = {}, options = {}, &block)
    options = {:tag => options} if options.is_a?(Symbol)

    prerender_options = options[:prerender]
    if prerender_options
      block = Proc.new{ concat React::ServerRendering.render(name, props, prerender_options) }
    end

    html_options = options.reverse_merge(:data => {})
    unless prerender_options == :static
      html_options[:data].tap do |data|
        data[:react_class] = name
        data[:react_props] = (props.is_a?(String) ? props : props.to_json)
      end
    end
    html_tag = html_options[:tag] || :div

    # remove internally used properties so they aren't rendered to DOM
    html_options.except!(:tag, :prerender, :camelize_props)

    content_tag(html_tag, '', html_options, &block)
  end
end
