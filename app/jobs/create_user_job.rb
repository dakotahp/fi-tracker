class CreateUserJob < ApplicationJob
  queue_as :default

  def perform(user)
    Goal.create!(
      net_worth_cents: 10000000,
      passive_income_cents: 1000000,
      household: user.household
    )

    UserSetting.create!(
      monthly_notification_enabled: true,
      notify_monthly_at: DateTime.now.utc.end_of_month,
      user: user
    )
  end
end
