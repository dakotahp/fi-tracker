class MonthlyReminderJob < ApplicationJob
  queue_as :default

  def perform
    subscribed_users.each do |user|
      if non_current_accounts(user).present?
        NotificationMailer.monthly_reminder(user).deliver_later
      end
    end
  end

  private
    def non_current_accounts(user)
      NonCurrentBalanceFinder.new(user).run
    end

    def subscribed_users
      User.joins(:user_setting).
        where(user_settings: {monthly_notification_enabled: true})
    end
end
