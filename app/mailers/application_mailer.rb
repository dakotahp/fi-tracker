class ApplicationMailer < ActionMailer::Base
  default from: "#{ENV['APP_NAME']} <dakotah@pena.name>"
  layout 'mailer'
end
