class NotificationMailer < ApplicationMailer
  include ApplicationHelper

  def monthly_reminder(user)
    @user = user
    @app_name = app_name
    mail(to: @user.email, subject: 'Reminder to update your balances')
  end
end
