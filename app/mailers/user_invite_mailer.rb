class UserInviteMailer < ApplicationMailer
  def invite(user_invite)
    @user_invite = user_invite
    @email = user_invite.email
    @link = new_user_registration_url(invite: @user_invite.uuid)

    mail(to: @email, subject: "You are invited to #{ENV['APP_NAME']}")
  end
end
