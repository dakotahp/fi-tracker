class Balance < ApplicationRecord
  include UuidToParam

  belongs_to :financial_account, optional: true
  monetize :amount_cents

  validates_presence_of :amount

  scope :newest_first, -> { order(recorded_on: :desc) }
  scope :newest_last, -> { order(recorded_on: :asc) }
  scope :by_month, -> {
    select("date_trunc( 'month', recorded_on ) as month, sum(amount_cents) as amount").group(:month)
  }
end
