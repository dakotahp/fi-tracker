module UuidToParam
  extend ActiveSupport::Concern

  included do
    before_create :set_uuid

    def set_uuid
      if uuid.blank?
        self.uuid = SecureRandom.uuid
      end
    end

    def to_param
      uuid
    end
  end
end
