class FinancialAccount < ApplicationRecord
  include UuidToParam

  ASSET = 'Asset'
  LIABILITY = 'Liability'

  KINDS = [
    ASSET,
    LIABILITY,
  ]

  AUTO_LOAN = 'auto_loan'
  CD = 'cd'
  CHECKING = 'checking'
  CREDIT_CARD = 'credit_card'
  LIFE_INSURANCE = 'life_insurance'
  METALS = 'metals'
  MONEY_MARKET = 'money_market'
  MORTGAGE = 'mortgage'
  OTHER_ASSETS = 'other_assets'
  OTHER_LIABILITIES = 'other_liabilities'
  REAL_ESTATE = 'real_estate'
  RETIREMENT = 'retirement'
  SAVINGS = 'savings'
  STOCKS_AND_BONDS = 'stocks_and_bonds'

  ASSETS = [
    CD,
    CHECKING,
    LIFE_INSURANCE,
    METALS,
    MONEY_MARKET,
    OTHER_ASSETS,
    REAL_ESTATE,
    RETIREMENT,
    SAVINGS,
    STOCKS_AND_BONDS,
  ]

  LIABILITIES = [
    AUTO_LOAN,
    CREDIT_CARD,
    MORTGAGE,
    OTHER_LIABILITIES,
  ]

  validates_presence_of :subtype

  has_many :balances
  has_many :financial_account_users
  has_many :users, through: :financial_account_users
  has_one :household_financial_account
  has_one :household, through: :household_financial_account

  scope :active, -> { where(closed: false) }
  scope :cash, -> { where(subtype: [CHECKING, MONEY_MARKET, SAVINGS]) }
  scope :liabilities, -> { where(type: LIABILITY) }
  scope :retirement, -> { where(subtype: [RETIREMENT]) }

  after_initialize :set_type_from_subtype, if: :no_type_set?

  accepts_nested_attributes_for :balances,
    allow_destroy: true,
    reject_if: :all_blank

  def balance
    balances.newest_last.last
  end

  def balance_amount
    balance.amount_cents / 100
  end

  def days_since_last_update
    (Date.current - balance.recorded_on).to_i
  end

  def updated_balance_needed?
    (Date.current - balance.recorded_on).to_i >= 30
  end

  def updated_in_current_month?
    balance.recorded_on.strftime("%Y-%m") == Date.current.strftime("%Y-%m")
  end

  private

  def set_type_from_subtype
    if ASSETS.include? subtype
      self.type = ASSET
    else
      self.type = LIABILITY
    end
  end

  def no_type_set?
    type.blank?
  end
end
