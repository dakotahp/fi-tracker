class FinancialAccountUser < ApplicationRecord
  belongs_to :user
  belongs_to :financial_account
end
