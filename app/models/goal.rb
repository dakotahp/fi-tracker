class Goal < ApplicationRecord
  monetize :net_worth_cents, :passive_income_cents
  belongs_to :household
end
