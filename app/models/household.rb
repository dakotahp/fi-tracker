class Household < ApplicationRecord
  include UuidToParam

  has_many :household_users
  has_many :users, through: :household_users
  has_many :household_financial_accounts
  has_many :active_financial_accounts,
    -> { where(closed: false) },
    through: :household_financial_accounts,
    source: :financial_account
  has_many :financial_accounts, through: :household_financial_accounts
  has_many :balances, through: :active_financial_accounts
  has_many :household_income_sources
  has_many :income_sources, through: :household_income_sources
  has_many :user_invites

  has_one :goal

  monetize :annual_income_cents

  def monthly_income_cents
    return 0 if annual_income_cents == 0
    annual_income_cents / 12
  end
end
