class HouseholdFinancialAccount < ApplicationRecord
  belongs_to :household
  belongs_to :financial_account
end
