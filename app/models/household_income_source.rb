class HouseholdIncomeSource < ApplicationRecord
  belongs_to :household
  belongs_to :income_source
end
