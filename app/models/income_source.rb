class IncomeSource < ApplicationRecord
  monetize :monthly_amount_cents

  has_one :household_income_source
  has_one :household, through: :household_income_source

  scope :alpha, -> { order(name: :asc) }
end
