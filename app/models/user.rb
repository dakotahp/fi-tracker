class User < ApplicationRecord
  include UuidToParam

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :confirmable,
    :database_authenticatable,
    :lockable,
    :registerable,
    :recoverable,
    :rememberable,
    :trackable,
    :validatable

  has_one :household_user, dependent: :destroy
  has_one :household, through: :household_user, dependent: :destroy
  has_one :user_setting, dependent: :destroy

  has_many :financial_account_users, dependent: :destroy
  has_many :financial_accounts, through: :financial_account_users, dependent: :destroy
  has_many :user_invites, dependent: :destroy

  before_create :generate_authentication_token

  attr_accessor :invite

  def authentication_token_string
    "Token token=#{authentication_token}, email=#{email}"
  end

  def destroy_authentication_token!
    self.update(authentication_token: nil)
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def generate_authentication_token!
    self.update(authentication_token: SecureRandom.base64(64))
  end

  def goal
    household.goal
  end

  private

  def generate_authentication_token
    self.authentication_token = SecureRandom.base64(64)
  end
end
