class UserInvite < ApplicationRecord
  include AASM
  include UuidToParam

  validates :email, presence: true
  validates_email_format_of :email

  belongs_to :user
  belongs_to :household

  before_update :set_status_updated_at, if: :status_changed?

  scope :invited, -> { where(status: STATE_INVITED) }

  aasm column: :status do
    state :invited, initial: true
    state :revoked
    state :accepted
  end

  private

    def set_status_updated_at
      self.status_updated_at = DateTime.now
    end
end
