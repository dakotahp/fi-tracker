class API::V1::BalanceResource < API::V1::BaseResource
  include MoneyRails::ActionViewExtension
  include Rails.application.routes.url_helpers
  default_url_options[:host] =
    Rails.application.config.action_mailer.default_url_options[:host]
  model_name 'Balance'

  attributes :amount, :created, :financial_account_uuid, :recorded_on

  def amount
    money_without_cents_and_with_symbol @model.amount
  end

  def created
    I18n.l(@model.recorded_on, format: :short)
  end

  def custom_links(options)
    {
      self_edit: edit_financial_account_balance_url(@model.financial_account, @model),
    }
  end

  def financial_account_uuid=(value)
    financial_account = FinancialAccount.find_by_uuid(value)
    @model.financial_account_id = financial_account.id
  end

  def financial_account_uuid
    @model.financial_account&.uuid
  end
end
