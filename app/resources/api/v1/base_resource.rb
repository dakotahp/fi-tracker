class API::V1::BaseResource < JSONAPI::Resource
  attribute :uuid
  key_type :uuid

  def id
    uuid
  end

  def self.all_records(_options = {})
    _model_class.all
  end

  def self.find_by_key(key, options = {})
    context = options[:context]
    records = all_records(options)
    records = apply_includes(records, options)
    model = records.find_by(uuid: key)
    fail JSONAPI::Exceptions::RecordNotFound, key if model.nil?
    resource_for_model(model).new(model, context)
  end
end
