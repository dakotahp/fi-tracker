class API::V1::FinancialAccountResource < API::V1::BaseResource
  model_name 'FinancialAccount'
  has_many :balances

  def self.records(options = {})
    options[:context][:current_user].financial_accounts
  end
end
