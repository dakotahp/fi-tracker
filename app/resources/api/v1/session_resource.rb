class API::V1::SessionResource < API::V1::BaseResource
  attributes :email, :first_name, :last_name, :token
  model_name "User"

  def token
    @model.authentication_token
  end
end
