class AcceptInvite
  attr_reader :user, :household_user, :invite

  def initialize(user, invite_uuid)
    @user = user
    @invite = UserInvite.find_by_uuid(invite_uuid)
  end

  def run
    # Update invite record to accepted
    invite.update!(status: UserInvite::STATE_ACCEPTED)

    # Add user to household
    @household_user = HouseholdUser.create!(
      user: user,
      household: invite.household
    )
  end
end
