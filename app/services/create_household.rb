class CreateHousehold
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def run
    household = Household.create(name: "#{user.email} Household")
    HouseholdUser.create(household: household, user: user)
  end
end
