class NonCurrentBalanceFinder
  attr_reader :current_user

  def initialize(current_user)
    @current_user = current_user
  end

  def run
    current_user.household.financial_accounts.reject{
      |fa| fa.updated_in_current_month?
    }
  end
end
