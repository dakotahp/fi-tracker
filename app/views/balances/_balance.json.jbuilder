json.extract! balance, :id, :amount_cents, :financial_account_id, :created_at, :updated_at
json.url balance_url(balance, format: :json)