json.extract! goal, :id, :net_worth_cents, :passive_income_cents, :household_id, :created_at, :updated_at
json.url goal_url(goal, format: :json)