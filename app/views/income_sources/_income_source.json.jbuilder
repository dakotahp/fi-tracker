json.extract! income_source, :id, :name, :monthly_amount_cents, :created_at, :updated_at
json.url income_source_url(income_source, format: :json)