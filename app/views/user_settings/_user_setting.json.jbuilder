json.extract! user_setting, :id, :monthly_notification_enabled, :notify_monthly_at, :annual_income_amount, :created_at, :updated_at
json.url user_setting_url(user_setting, format: :json)
