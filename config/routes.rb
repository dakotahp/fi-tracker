Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      jsonapi_resources :balances, only: [:create, :destroy, :update]
      jsonapi_resources :financial_accounts, only: [:show]
      resource :sessions, only: [:create, :destroy]
    end
  end

  resources :households, only: :update
  resources :user_settings, only: [:update]
  resources :settings, only: [:index]
  resources :income_sources, only: [:create, :edit, :destroy, :index, :update, :new]
  resources :goals, only: [:index, :update]

  resources :assets, only: :show
  resources :liabilities, only: :show
  resources :financial_accounts do
    resources :balances, only: [:create, :destroy, :edit, :index, :show, :update]
    resources :financial_account_users, only: [:create, :destroy]
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :user_invites, only: [:create, :destroy] do
    member do
      put :resend
    end
  end

  get '/dashboard', controller: :dashboards, action: :index

  get "/pages/*id" => 'pages#show', as: :pages, format: false
  root 'pages#show', id: 'homepage'
end
