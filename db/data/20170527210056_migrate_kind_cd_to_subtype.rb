class MigrateKindCdToSubtype < ActiveRecord::Migration[5.1]
  def self.up
    kinds = [
      'checking',
      'savings',
      'cd',
      'retirement',
      'money_market',
      'credit',
      'stocks_and_bonds',
    ]
    FinancialAccount.all.each do |financial_account|
      if financial_account.kind_cd.present?
        financial_account.update(subtype: kinds[financial_account.kind_cd])
      end
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
