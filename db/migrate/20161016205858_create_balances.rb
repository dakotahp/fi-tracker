class CreateBalances < ActiveRecord::Migration[5.0]
  def change
    create_table :balances do |t|
      t.integer :amount_cents
      t.integer :financial_account_id

      t.timestamps
    end
  end
end
