class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.integer :net_worth_cents
      t.integer :passive_income_cents
      t.integer :household_id

      t.timestamps
    end
  end
end
