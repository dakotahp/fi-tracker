class CreateHouseholdUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :household_users do |t|
      t.integer :user_id
      t.integer :household_id
      t.timestamps
    end

    add_index :household_users, :household_id
    add_index :household_users, :user_id
  end
end
