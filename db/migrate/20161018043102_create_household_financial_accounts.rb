class CreateHouseholdFinancialAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :household_financial_accounts do |t|
      t.integer :household_id
      t.integer :financial_account_id
      t.timestamps
    end

    add_index :household_financial_accounts, :household_id
    add_index :household_financial_accounts, :financial_account_id
  end
end
