class CreateIncomeSources < ActiveRecord::Migration[5.0]
  def change
    create_table :income_sources do |t|
      t.string :name
      t.integer :monthly_amount_cents

      t.timestamps
    end
  end
end
