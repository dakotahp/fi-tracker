class CreateHouseholdIncomeSources < ActiveRecord::Migration[5.0]
  def change
    create_table :household_income_sources do |t|
      t.integer :household_id
      t.integer :income_source_id

      t.timestamps
    end
  end
end
