class AddRecordedOnToBalances < ActiveRecord::Migration[5.0]
  def change
    add_column :balances, :recorded_on, :date
  end
end
