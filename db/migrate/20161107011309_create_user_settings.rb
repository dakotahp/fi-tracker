class CreateUserSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :user_settings do |t|
      t.boolean :monthly_notification_enabled
      t.datetime :notify_monthly_at
      t.integer :monthly_income_cents
      t.integer :user_id

      t.timestamps
    end
  end
end
