class CreateUserInvites < ActiveRecord::Migration[5.0]
  def change
    enable_extension "uuid-ossp"

    create_table :user_invites do |t|
      t.uuid :uuid, default: "uuid_generate_v4()", null: false
      t.string :email
      t.string :status
      t.datetime :status_updated_at
      t.integer :household_id
      t.integer :user_id

      t.timestamps
    end
  end
end
