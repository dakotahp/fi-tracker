class CreateFinancialAccountUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_account_users do |t|
      t.integer :user_id
      t.integer :financial_account_id

      t.timestamps
    end
  end
end
