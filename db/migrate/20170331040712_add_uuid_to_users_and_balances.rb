class AddUuidToUsersAndBalances < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :uuid, :uuid, default: "uuid_generate_v4()", null: false
    add_column :balances, :uuid, :uuid, default: "uuid_generate_v4()", null: false

    add_index :users, :uuid, unique: true
    add_index :balances, :uuid, unique: true
  end
end
