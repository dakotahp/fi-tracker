class AddUuidToFinancialAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :financial_accounts, :uuid, :uuid, default: "uuid_generate_v4()", null: false
    add_index :financial_accounts, :uuid, unique: true
  end
end
