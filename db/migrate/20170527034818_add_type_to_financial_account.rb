class AddTypeToFinancialAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :financial_accounts, :type, :string
  end
end
