class AddSubtypeToFinancialAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :financial_accounts, :subtype, :string
  end
end
