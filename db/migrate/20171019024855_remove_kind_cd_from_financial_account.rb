class RemoveKindCdFromFinancialAccount < ActiveRecord::Migration[5.0]
  def change
    remove_column :financial_accounts, :kind_cd
  end
end
