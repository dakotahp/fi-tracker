class RenameMonthlyIncomeToYearlyIncome < ActiveRecord::Migration[5.1]
  def change
    rename_column :user_settings, :monthly_income_cents, :annual_income_cents
  end
end
