class MoveAnnualIncomeCentsToHousehold < ActiveRecord::Migration[5.1]
  def change
    remove_column :user_settings, :annual_income_cents
    add_column :households, :annual_income_cents, :integer, default: 0
  end
end
