class AddUuidToHouseholds < ActiveRecord::Migration[5.1]
  def change
    add_column :households, :uuid, :uuid, default: "uuid_generate_v4()", null: false
    add_index :households, :uuid, unique: true
  end
end
