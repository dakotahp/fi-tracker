# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180902000404) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "balances", id: :serial, force: :cascade do |t|
    t.integer "amount_cents"
    t.integer "financial_account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "recorded_on"
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.index ["uuid"], name: "index_balances_on_uuid", unique: true
  end

  create_table "financial_account_users", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "financial_account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_accounts", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.string "type"
    t.string "subtype"
    t.boolean "closed", default: false
    t.index ["uuid"], name: "index_financial_accounts_on_uuid", unique: true
  end

  create_table "goals", id: :serial, force: :cascade do |t|
    t.integer "net_worth_cents"
    t.integer "passive_income_cents"
    t.integer "household_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "household_financial_accounts", id: :serial, force: :cascade do |t|
    t.integer "household_id"
    t.integer "financial_account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["financial_account_id"], name: "index_household_financial_accounts_on_financial_account_id"
    t.index ["household_id"], name: "index_household_financial_accounts_on_household_id"
  end

  create_table "household_income_sources", id: :serial, force: :cascade do |t|
    t.integer "household_id"
    t.integer "income_source_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "household_users", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "household_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["household_id"], name: "index_household_users_on_household_id"
    t.index ["user_id"], name: "index_household_users_on_user_id"
  end

  create_table "households", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "annual_income_cents", default: 0
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.index ["uuid"], name: "index_households_on_uuid", unique: true
  end

  create_table "income_sources", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "monthly_amount_cents"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_invites", id: :serial, force: :cascade do |t|
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.string "email"
    t.string "status"
    t.datetime "status_updated_at"
    t.integer "household_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_settings", id: :serial, force: :cascade do |t|
    t.boolean "monthly_notification_enabled"
    t.datetime "notify_monthly_at"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token"
    t.string "first_name"
    t.string "last_name"
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["uuid"], name: "index_users_on_uuid", unique: true
  end

end
