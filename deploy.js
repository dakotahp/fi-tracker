if (process.env.NODE_ENV === "production") {
  var child_process = require("child_process");
  var build = "webpack --config ./webpack.prod.config.js --progress --colors";
  child_process.exec(build, function (error, stdout, stderr) {
    console.log("stdout: " + stdout);
    console.log("stderr: " + stderr);
    if (error !== null) {
      console.log("exec error: " + error);
    }
  });
} else {
  console.log("> Not production, skipping deploy script");
}
