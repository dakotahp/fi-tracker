namespace :deploy do
  desc 'Deploy to production'
  task :production do
    app = "fi-tracker"
    remote = "git@heroku.com:#{app}.git"

    system "heroku maintenance:on --app #{app}"
    system "git push #{remote} master"
    system "heroku run rake db:migrate --app #{app}"
    system "heroku run rake data:migrate --app #{app}"
    system "heroku maintenance:off --app #{app}"
  end

  task :staging do
    app = "fi-tracker-staging"
    remote = "git@heroku.com:#{app}.git"

    system "git push #{remote} master"
    system "heroku run rake db:migrate --app #{app}"
    system "heroku run rake data:migrate --app #{app}"
  end
end
