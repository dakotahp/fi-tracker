namespace :monthly_reminders do
  desc "Sends user monthly reminder to update balances."
  task notify: :environment do
    MonthlyReminderJob.perform_now
  end
end
