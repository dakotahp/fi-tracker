require 'rails_helper'

RSpec.describe BalancesController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Balance. As you add validations to Balance, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    {amount: 100}
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # BalancesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    skip it "assigns all balances as @balances" do
      balance = Balance.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:balances)).to eq([balance])
    end
  end

  describe "GET #show" do
    skip it "assigns the requested balance as @balance" do
      balance = Balance.create! valid_attributes
      get :show, params: {id: balance.to_param}, session: valid_session
      expect(assigns(:balance)).to eq(balance)
    end
  end

  describe "GET #new" do
    skip it "assigns a new balance as @balance" do
      get :new, params: {}, session: valid_session
      expect(assigns(:balance)).to be_a_new(Balance)
    end
  end

  describe "GET #edit" do
    skip it "assigns the requested balance as @balance" do
      balance = Balance.create! valid_attributes
      get :edit, params: {id: balance.to_param}, session: valid_session
      expect(assigns(:balance)).to eq(balance)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      skip it "creates a new Balance" do
        expect {
          post :create, params: {balance: valid_attributes}, session: valid_session
        }.to change(Balance, :count).by(1)
      end

      skip it "assigns a newly created balance as @balance" do
        post :create, params: {balance: valid_attributes}, session: valid_session
        expect(assigns(:balance)).to be_a(Balance)
        expect(assigns(:balance)).to be_persisted
      end

      skip it "redirects to the created balance" do
        post :create, params: {balance: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Balance.last)
      end
    end

    context "with invalid params" do
      skip it "assigns a newly created but unsaved balance as @balance" do
        post :create, params: {balance: invalid_attributes}, session: valid_session
        expect(assigns(:balance)).to be_a_new(Balance)
      end

      skip it "re-renders the 'new' template" do
        post :create, params: {balance: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      skip it "updates the requested balance" do
        balance = Balance.create! valid_attributes
        put :update, params: {id: balance.to_param, balance: new_attributes}, session: valid_session
        balance.reload
        skip("Add assertions for updated state")
      end

      skip it "assigns the requested balance as @balance" do
        balance = Balance.create! valid_attributes
        put :update, params: {id: balance.to_param, balance: valid_attributes}, session: valid_session
        expect(assigns(:balance)).to eq(balance)
      end

      skip it "redirects to the balance" do
        balance = Balance.create! valid_attributes
        put :update, params: {id: balance.to_param, balance: valid_attributes}, session: valid_session
        expect(response).to redirect_to(balance)
      end
    end

    context "with invalid params" do
      skip it "assigns the balance as @balance" do
        balance = Balance.create! valid_attributes
        put :update, params: {id: balance.to_param, balance: invalid_attributes}, session: valid_session
        expect(assigns(:balance)).to eq(balance)
      end

      skip it "re-renders the 'edit' template" do
        balance = Balance.create! valid_attributes
        put :update, params: {id: balance.to_param, balance: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    skip it "destroys the requested balance" do
      balance = Balance.create! valid_attributes
      expect {
        delete :destroy, params: {id: balance.to_param}, session: valid_session
      }.to change(Balance, :count).by(-1)
    end

    skip it "redirects to the balances list" do
      balance = Balance.create! valid_attributes
      delete :destroy, params: {id: balance.to_param}, session: valid_session
      expect(response).to redirect_to(balances_url)
    end
  end

end
