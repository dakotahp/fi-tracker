require 'rails_helper'

RSpec.describe FinancialAccountsController, type: :controller do
  login_user

  let(:valid_attributes) {
    {
      name: 'Ally',
      subtype: 'checking',
      balances_attributes: [
        amount: 10000
      ]
    }
  }

  let(:invalid_attributes) {
    {
      name: '',
      subtype: 'checking',
      balances_attributes: [
        amount: nil
      ]
    }
  }
  #
  # let :user do
  #   User.create!(email: "me@home.com", password: "watching the telly")
  # end
  #
  # before { sign_in user }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # FinancialAccountsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    skip it "assigns all financial_accounts as @financial_accounts" do
      financial_account = FinancialAccount.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:financial_accounts)).to eq([financial_account])
    end
  end

  describe "GET #show" do
    skip it "assigns the requested financial_account as @financial_account" do
      financial_account = FinancialAccount.create! valid_attributes
      get :show, params: {id: financial_account.to_param}, session: valid_session
      expect(assigns(:financial_account)).to eq(financial_account)
    end
  end

  describe "GET #new" do
    skip it "assigns a new financial_account as @financial_account" do
      get :new, params: {}, session: valid_session
      expect(assigns(:financial_account)).to be_a_new(FinancialAccount)
    end
  end

  describe "GET #edit" do
    skip it "assigns the requested financial_account as @financial_account" do
      financial_account = FinancialAccount.create! valid_attributes
      get :edit, params: {id: financial_account.to_param}, session: valid_session
      expect(assigns(:financial_account)).to eq(financial_account)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new FinancialAccount" do
        post :create, params: {financial_account: valid_attributes}, session: valid_session

        expect(FinancialAccount.last.balance.recorded_on).to eq(Date.current)
        expect(FinancialAccount.all.size).to eq(1)
      end

      skip it "assigns a newly created financial_account as @financial_account" do
        post :create, params: {financial_account: valid_attributes}, session: valid_session
        expect(assigns(:financial_account)).to be_a(FinancialAccount)
        expect(assigns(:financial_account)).to be_persisted
      end

      skip it "redirects to the created financial_account" do
        post :create, params: {financial_account: valid_attributes}, session: valid_session
        expect(response).to redirect_to(FinancialAccount.last)
      end
    end

    context "with invalid params" do
      skip it "assigns a newly created but unsaved financial_account as @financial_account" do
        post :create, params: {financial_account: invalid_attributes}, session: valid_session
        expect(assigns(:financial_account)).to be_a_new(FinancialAccount)
      end

      skip it "re-renders the 'new' template" do
        post :create, params: {financial_account: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      skip it "updates the requested financial_account" do
        financial_account = FinancialAccount.create! valid_attributes
        put :update, params: {id: financial_account.to_param, financial_account: new_attributes}, session: valid_session
        financial_account.reload
        skip("Add assertions for updated state")
      end

      skip it "assigns the requested financial_account as @financial_account" do
        financial_account = FinancialAccount.create! valid_attributes
        put :update, params: {id: financial_account.to_param, financial_account: valid_attributes}, session: valid_session
        expect(assigns(:financial_account)).to eq(financial_account)
      end

      skip it "redirects to the financial_account" do
        financial_account = FinancialAccount.create! valid_attributes
        put :update, params: {id: financial_account.to_param, financial_account: valid_attributes}, session: valid_session
        expect(response).to redirect_to(financial_account)
      end
    end

    context "with invalid params" do
      skip it "assigns the financial_account as @financial_account" do
        financial_account = FinancialAccount.create! valid_attributes
        put :update, params: {id: financial_account.to_param, financial_account: invalid_attributes}, session: valid_session
        expect(assigns(:financial_account)).to eq(financial_account)
      end

      skip it "re-renders the 'edit' template" do
        financial_account = FinancialAccount.create! valid_attributes
        put :update, params: {id: financial_account.to_param, financial_account: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    skip it "destroys the requested financial_account" do
      financial_account = FinancialAccount.create! valid_attributes
      expect {
        delete :destroy, params: {id: financial_account.to_param}, session: valid_session
      }.to change(FinancialAccount, :count).by(-1)
    end

    skip it "redirects to the financial_accounts list" do
      financial_account = FinancialAccount.create! valid_attributes
      delete :destroy, params: {id: financial_account.to_param}, session: valid_session
      expect(response).to redirect_to(financial_accounts_url)
    end
  end

end
