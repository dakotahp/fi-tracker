require 'rails_helper'

RSpec.describe GoalsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Goal. As you add validations to Goal, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # GoalsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    skip it "assigns all goals as @goals" do
      goal = Goal.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:goals)).to eq([goal])
    end
  end

  describe "GET #show" do
    skip it "assigns the requested goal as @goal" do
      goal = Goal.create! valid_attributes
      get :show, params: {id: goal.to_param}, session: valid_session
      expect(assigns(:goal)).to eq(goal)
    end
  end

  describe "GET #new" do
    skip it "assigns a new goal as @goal" do
      get :new, params: {}, session: valid_session
      expect(assigns(:goal)).to be_a_new(Goal)
    end
  end

  describe "GET #edit" do
    skip it "assigns the requested goal as @goal" do
      goal = Goal.create! valid_attributes
      get :edit, params: {id: goal.to_param}, session: valid_session
      expect(assigns(:goal)).to eq(goal)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      skip it "creates a new Goal" do
        expect {
          post :create, params: {goal: valid_attributes}, session: valid_session
        }.to change(Goal, :count).by(1)
      end

      skip it "assigns a newly created goal as @goal" do
        post :create, params: {goal: valid_attributes}, session: valid_session
        expect(assigns(:goal)).to be_a(Goal)
        expect(assigns(:goal)).to be_persisted
      end

      skip it "redirects to the created goal" do
        post :create, params: {goal: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Goal.last)
      end
    end

    context "with invalid params" do
      skip it "assigns a newly created but unsaved goal as @goal" do
        post :create, params: {goal: invalid_attributes}, session: valid_session
        expect(assigns(:goal)).to be_a_new(Goal)
      end

      skip it "re-renders the 'new' template" do
        post :create, params: {goal: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      skip it "updates the requested goal" do
        goal = Goal.create! valid_attributes
        put :update, params: {id: goal.to_param, goal: new_attributes}, session: valid_session
        goal.reload
        skip("Add assertions for updated state")
      end

      skip it "assigns the requested goal as @goal" do
        goal = Goal.create! valid_attributes
        put :update, params: {id: goal.to_param, goal: valid_attributes}, session: valid_session
        expect(assigns(:goal)).to eq(goal)
      end

      skip it "redirects to the goal" do
        goal = Goal.create! valid_attributes
        put :update, params: {id: goal.to_param, goal: valid_attributes}, session: valid_session
        expect(response).to redirect_to(goal)
      end
    end

    context "with invalid params" do
      skip it "assigns the goal as @goal" do
        goal = Goal.create! valid_attributes
        put :update, params: {id: goal.to_param, goal: invalid_attributes}, session: valid_session
        expect(assigns(:goal)).to eq(goal)
      end

      skip it "re-renders the 'edit' template" do
        goal = Goal.create! valid_attributes
        put :update, params: {id: goal.to_param, goal: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    skip it "destroys the requested goal" do
      goal = Goal.create! valid_attributes
      expect {
        delete :destroy, params: {id: goal.to_param}, session: valid_session
      }.to change(Goal, :count).by(-1)
    end

    skip it "redirects to the goals list" do
      goal = Goal.create! valid_attributes
      delete :destroy, params: {id: goal.to_param}, session: valid_session
      expect(response).to redirect_to(goals_url)
    end
  end

end
