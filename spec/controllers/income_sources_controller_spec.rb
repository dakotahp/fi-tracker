require 'rails_helper'

RSpec.describe IncomeSourcesController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # IncomeSource. As you add validations to IncomeSource, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # IncomeSourcesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    skip it "assigns all income_sources as @income_sources" do
      income_source = IncomeSource.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:income_sources)).to eq([income_source])
    end
  end

  describe "GET #show" do
    skip it "assigns the requested income_source as @income_source" do
      income_source = IncomeSource.create! valid_attributes
      get :show, params: {id: income_source.to_param}, session: valid_session
      expect(assigns(:income_source)).to eq(income_source)
    end
  end

  describe "GET #new" do
    skip it "assigns a new income_source as @income_source" do
      get :new, params: {}, session: valid_session
      expect(assigns(:income_source)).to be_a_new(IncomeSource)
    end
  end

  describe "GET #edit" do
    skip it "assigns the requested income_source as @income_source" do
      income_source = IncomeSource.create! valid_attributes
      get :edit, params: {id: income_source.to_param}, session: valid_session
      expect(assigns(:income_source)).to eq(income_source)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      skip it "creates a new IncomeSource" do
        expect {
          post :create, params: {income_source: valid_attributes}, session: valid_session
        }.to change(IncomeSource, :count).by(1)
      end

      skip it "assigns a newly created income_source as @income_source" do
        post :create, params: {income_source: valid_attributes}, session: valid_session
        expect(assigns(:income_source)).to be_a(IncomeSource)
        expect(assigns(:income_source)).to be_persisted
      end

      skip it "redirects to the created income_source" do
        post :create, params: {income_source: valid_attributes}, session: valid_session
        expect(response).to redirect_to(IncomeSource.last)
      end
    end

    context "with invalid params" do
      skip it "assigns a newly created but unsaved income_source as @income_source" do
        post :create, params: {income_source: invalid_attributes}, session: valid_session
        expect(assigns(:income_source)).to be_a_new(IncomeSource)
      end

      skip it "re-renders the 'new' template" do
        post :create, params: {income_source: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      skip it "updates the requested income_source" do
        income_source = IncomeSource.create! valid_attributes
        put :update, params: {id: income_source.to_param, income_source: new_attributes}, session: valid_session
        income_source.reload
        skip("Add assertions for updated state")
      end

      skip it "assigns the requested income_source as @income_source" do
        income_source = IncomeSource.create! valid_attributes
        put :update, params: {id: income_source.to_param, income_source: valid_attributes}, session: valid_session
        expect(assigns(:income_source)).to eq(income_source)
      end

      skip it "redirects to the income_source" do
        income_source = IncomeSource.create! valid_attributes
        put :update, params: {id: income_source.to_param, income_source: valid_attributes}, session: valid_session
        expect(response).to redirect_to(income_source)
      end
    end

    context "with invalid params" do
      skip it "assigns the income_source as @income_source" do
        income_source = IncomeSource.create! valid_attributes
        put :update, params: {id: income_source.to_param, income_source: invalid_attributes}, session: valid_session
        expect(assigns(:income_source)).to eq(income_source)
      end

      skip it "re-renders the 'edit' template" do
        income_source = IncomeSource.create! valid_attributes
        put :update, params: {id: income_source.to_param, income_source: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    skip it "destroys the requested income_source" do
      income_source = IncomeSource.create! valid_attributes
      expect {
        delete :destroy, params: {id: income_source.to_param}, session: valid_session
      }.to change(IncomeSource, :count).by(-1)
    end

    skip it "redirects to the income_sources list" do
      income_source = IncomeSource.create! valid_attributes
      delete :destroy, params: {id: income_source.to_param}, session: valid_session
      expect(response).to redirect_to(income_sources_url)
    end
  end

end
