require 'rails_helper'

RSpec.describe UserInvitesController, type: :controller do
  include ActiveJob::TestHelper
  login_user

  let(:valid_attributes) {
    {email: "user@email.com"}
  }

  let(:invalid_attributes) {
    {email: ""}
  }

  let(:valid_session) { {username: "username", password: "password"} }

  describe "POST #create" do
    context "with valid params" do
      it "creates a new UserInvite" do
        expect {
          post :create, params: {user_invite: valid_attributes}, session: valid_session
        }.to change(UserInvite, :count).by(1)
      end

      skip it "sends an invite email" do
        perform_enqueued_jobs do
          post :create, params: {user_invite: valid_attributes}, session: valid_session
        end

        # expect(ActionMailer::Base.deliveries.size).to_change { ActionMailer::Base.deliveries.size }.by(1)
      end

      it "assigns a newly created user_invite as @user_invite" do
        post :create, params: {user_invite: valid_attributes}, session: valid_session
        expect(assigns(:user_invite)).to be_a(UserInvite)
        expect(assigns(:user_invite)).to be_persisted
      end

      it "redirects to the user_settings page" do
        post :create, params: {user_invite: valid_attributes}, session: valid_session
        expect(response).to redirect_to(settings_path)
      end
    end

    context "with invalid params" do
      skip it "assigns a newly created but unsaved user_invite as @user_invite" do
        post :create, params: {user_invite: attributes_for(:invalid_user_invite)}
        expect(assigns(:user_invite)).to be_a_new(UserInvite)
      end
    end
  end

  describe "PUT #resend" do
    context "with valid params" do
      skip it "should resend an invite email" do
        user = User.first
        household = create(:household)
        user_invite = create(:user_invite, user: user, household: household)

        expect do
          perform_enqueued_jobs do
            put :resend, params: {id: user_invite.uuid}, session: valid_session
          end
        end.to change { ActionMailer::Base.deliveries.size }.by(1)
      end
    end
  end
end
