require 'rails_helper'

RSpec.describe HouseholdNetWorths, type: :facade do
  attr_reader :facade, :goal, :household

  before(:each) do
    @household = create(:household, :with_user)
    financial_account = create(:financial_account, :with_balance)
    create(
      :household_financial_account,
      household: household,
      financial_account: financial_account
    )
    @goal = create(:goal, household: household)

    @facade = HouseholdNetWorths.new(household)
  end

  it "should return net worth" do
    expect(facade.net_worth.to_i).to eq(100)
    expect(facade.net_worth.class).to eq(Money)
  end

  it "should return goal progress" do
    expect(facade.goal_progress.to_i).to eq(10)
    expect(facade.goal_progress.class).to eq(Money)
  end
end
