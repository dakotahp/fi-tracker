require 'rails_helper'

RSpec.describe NetWorth, type: :facade do
  attr_reader :facade, :financial_account, :household

  before(:each) do
    @household = create(:household, :with_user)
    @financial_account = create(:financial_account, :with_balance)
    @original_balance = financial_account.balances.first
    create(
      :household_financial_account,
      household: household,
      financial_account: financial_account
    )
  end

  describe "#amounts" do
    it "should exclude other households" do
      household_2 = create(:household, :with_user)
      financial_account_2 = create(:financial_account, :with_balance)
      create(
        :household_financial_account,
        household: household_2,
        financial_account: financial_account_2
      )

      @facade = NetWorth.new(household)

      financial_account_2.balances.last.update(amount_cents: 11111)
      expect(facade.amounts).to eq([100])
    end

    it "return amounts of results" do
      decoy = create(:balance, financial_account: financial_account)
      previous_balance = create(
        :balance,
        recorded_on: 1.month.ago,
        financial_account: financial_account
      )

      @facade = NetWorth.new(household)

      expect(facade.amounts.size).to eq(2)
      expect(facade.amounts.first).to eq(previous_balance.amount_cents / 100)
      expect(facade.amounts.last).to eq(
        (decoy.amount_cents + @original_balance.amount_cents) / 100
      )
    end
  end

  describe "#average_increase" do
    it "should return average increase between each pair of net worths" do
      allow_any_instance_of(NetWorth).to receive(:amount_cents).
        and_return([20000,40000,60000,80000])

      @facade = NetWorth.new(household)

      expect(facade.average_increase).to eq(200)
    end
  end

  describe "#balances" do
    it "should group balances by month" do
      second_balance = create(:balance, financial_account: financial_account)
      decoy = create(
        :balance,
        recorded_on: 1.month.ago,
        financial_account: financial_account
      )

      @facade = NetWorth.new(household)

      expect(facade.balances.to_ary.size).to eq(2)
      expect(facade.balances.first.month.month).to eq(Date.current.month)
      expect(facade.balances.first.amount_cents).to eq(
        @original_balance.amount_cents + second_balance.amount_cents
      )

      expect(facade.balances.last.month.month).to eq(Date.current.month - 1)
      expect(facade.balances.last.amount_cents).to eq(
        decoy.amount_cents
      )
    end

    it "should reverse order of results with newest first" do
      create(
        :balance,
        recorded_on: 1.month.ago,
        financial_account: financial_account
      )

      @facade = NetWorth.new(household)

      expect(facade.balances.first.month.month).to eq(Date.current.month)
      expect(facade.balances.last.month.month).to eq(Date.current.month - 1)
    end

    it "should ignore balances of closed accounts" do
      second_balance = create(:balance, financial_account: financial_account)
      decoy = create(
        :balance,
        recorded_on: 1.month.ago,
        financial_account: financial_account
      )

      closed_financial_account = create(
        :financial_account,
        :with_balance,
        closed: true
      )

      closed_financial_account.balances.last.update!(
        amount_cents: 100000000
      )

      @facade = NetWorth.new(household)

      # must convert to array, query fails otherwise
      expect(facade.balances.to_ary.size).to eq(2)
      expect(facade.balances.first.month.month).to eq(Date.current.month)
      expect(facade.balances.first.amount_cents).to eq(
        @original_balance.amount_cents + second_balance.amount_cents
      )
    end
  end

  describe "#months" do
    it "return months of results" do
      create(:balance, financial_account: financial_account)
      create(
        :balance,
        recorded_on: 1.month.ago,
        financial_account: financial_account
      )

      @facade = NetWorth.new(household)

      expect(facade.months.size).to eq(2)
      expect(facade.months.first.month).to eq(Date.current.month - 1)
      expect(facade.months.last.month).to eq(Date.current.month)
    end
  end
end
