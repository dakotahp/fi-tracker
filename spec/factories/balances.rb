FactoryGirl.define do
  factory :balance do
    amount_cents 10000
    recorded_on Date.current
    financial_account
  end
end
