FactoryGirl.define do
  factory :financial_account_user do
    user
    financial_account
  end
end
