FactoryGirl.define do
  factory :financial_account do
    type "Asset"
    subtype :checking

    trait :with_balance do
      after(:create) do |financial_account|
        financial_account.reload
        create(:balance, financial_account: financial_account)
      end
    end
  end
end
