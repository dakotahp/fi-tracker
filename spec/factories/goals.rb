FactoryGirl.define do
  factory :goal do
    net_worth_cents 100000
    passive_income_cents 10000
    household
  end
end
