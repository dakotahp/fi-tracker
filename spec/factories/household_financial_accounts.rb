FactoryGirl.define do
  factory :household_financial_account do
    household
    financial_account
  end
end
