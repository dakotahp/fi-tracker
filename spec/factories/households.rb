FactoryGirl.define do
  factory :household do
    name "Joe Blow's Household"

    trait :with_user do
      after(:create) do |household|
        household.reload

        user = create(:user, email: "esnowden#{Random.rand(100)}@lavabit.com")
        create(:household_user, user: user, household: household)
      end
    end
  end
end
