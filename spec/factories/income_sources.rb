FactoryGirl.define do
  factory :income_source do
    name "Side Hustle"
    monthly_amount_cents 1
  end
end
