FactoryGirl.define do
  factory :user_invite do
    uuid { SecureRandom.uuid }
    email "newuser@email.com"
    status "invited"
    status_updated_at nil
    household
    user
  end

  factory :invalid_user_invite, parent: :user_invite do |f|
    f.uuid nil
    f.email "user@domaincom"
  end
end
