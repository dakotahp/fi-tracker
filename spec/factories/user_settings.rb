FactoryGirl.define do
  factory :user_setting do
    monthly_notification_enabled true
    notify_monthly_at "2016-11-06 17:13:09"
    annual_income_cents 10000
  end
end
