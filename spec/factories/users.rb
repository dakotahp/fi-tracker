FactoryGirl.define do
  factory :user do
    email "esnowden#{Random.rand(1000)}@lavabit.com"
    first_name "Edward"
    last_name "Snowden"
    password "password"
    authentication_token SecureRandom.base64(64)
    confirmed_at DateTime.current
  end
end
