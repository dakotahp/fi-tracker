require 'rails_helper'

describe MonthlyReminderJob, type: :job do
  include UsersHelper

  before(:each) do
    create_user_with_household

    user_without_notification = create_user_with_household
    user_without_notification.user_setting.update(monthly_notification_enabled: false)
  end

  describe "#perform" do
    skip it "should send to users with notification settings on" do
      allow(MonthlyReminderJob).to receive(:perform_now)
      allow(NotificationMailer).to receive_message_chain(
        :monthly_reminder,
        :deliver_later
      )

      expect(MonthlyReminderJob.new.perform_now.size).to eq(1)
    end
  end
end
