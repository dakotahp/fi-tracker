require 'rails_helper'

RSpec.describe Balance, type: :model do
  describe "Assocations" do
    it { should belong_to(:financial_account) }
  end

  it "create balance" do
    household = create(:household)
    goal = create(:goal, household: household)

    expect(Goal.last).to eq(goal)
    expect(Goal.last.household).to eq(household)
  end
end
