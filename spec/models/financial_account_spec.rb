require 'rails_helper'

RSpec.describe FinancialAccount, type: :model do
  describe "Assocations" do
    it { should have_many(:balances) }
    it { should have_many(:financial_account_users) }
    it { should have_many(:users) }

    it { should have_one(:household_financial_account) }
    it { should have_one(:household) } #
  end

  context "#days_since_last_update" do
    it "returns number of days since last balance update" do
      balance = create(:balance, recorded_on: 25.days.ago)
      financial_account = balance.financial_account
      expect(financial_account.days_since_last_update).to eq 25
    end
  end

  context "#updated_balance_needed?" do
    it "returns true if more than 30 days ago" do
      balance = create(:balance, recorded_on: 35.days.ago)
      financial_account = balance.financial_account
      expect(financial_account.updated_balance_needed?).to eq true
    end

    it "returns false if less than 30 days ago" do
      balance = create(:balance, recorded_on: 25.days.ago)
      financial_account = balance.financial_account
      expect(financial_account.updated_balance_needed?).to eq false
    end
  end

  context "#updated_in_current_month?" do
    it "returns true if balance in current month" do
      balance = create(:balance)
      financial_account = balance.financial_account
      expect(financial_account.updated_in_current_month?).to eq true
    end

    it "returns false if balance in previous month" do
      balance = create(:balance, recorded_on: 35.days.ago)
      financial_account = balance.financial_account
      expect(financial_account.updated_in_current_month?).to eq false
    end
  end
end
