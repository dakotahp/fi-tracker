require 'rails_helper'

RSpec.describe FinancialAccountUser, type: :model do
  describe "Assocations" do
    it { should belong_to(:user) }
    it { should belong_to(:financial_account) }
  end
end
