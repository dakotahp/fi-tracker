require 'rails_helper'

RSpec.describe Goal, type: :model do
  describe "Assocations" do
    it { should belong_to(:household) }
  end

  it "create goal" do
    household = create(:household)
    goal = create(:goal, household: household)

    expect(Goal.last).to eq(goal)
    expect(Goal.last.household).to eq(household)
  end
end
