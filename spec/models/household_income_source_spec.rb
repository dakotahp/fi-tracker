require 'rails_helper'

RSpec.describe HouseholdIncomeSource, type: :model do
  describe "Assocations" do
    it { should belong_to(:household) }
    it { should belong_to(:income_source) }
  end
end
