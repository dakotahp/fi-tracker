require 'rails_helper'

RSpec.describe Household, type: :model do
  describe "Assocations" do
    it { should have_many(:household_users) }
    it { should have_many(:users) }
    it { should have_many(:household_financial_accounts) }
    it { should have_many(:financial_accounts) }
    it { should have_many(:balances) }
    it { should have_many(:household_income_sources) }
    it { should have_many(:income_sources) }
    it { should have_many(:user_invites) }
    it { should have_one(:goal) }
  end
end
