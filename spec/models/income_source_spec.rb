require 'rails_helper'

RSpec.describe IncomeSource, type: :model do
  describe "Assocations" do
    it { should have_one(:household_income_source) }
    it { should have_one(:household) }
  end
end
