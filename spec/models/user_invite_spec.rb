require 'rails_helper'

RSpec.describe UserInvite, type: :model do
  describe "Assocations" do
    it { should belong_to(:household) }
    it { should belong_to(:user) }
  end

  describe "Validations" do
    it { should validate_presence_of(:email) }
  end

  context "#create" do
    it "raises an error if email is invalid" do
      expect { create(:invalid_user_invite) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context "#update" do
    before(:each) do
      @user_invite = create(:user_invite)
    end

    it "raises an error if email is invalid" do
      expect { @user_invite.update!(email: "")}.
        to raise_error(ActiveRecord::RecordInvalid)
      expect { @user_invite.update!(email: "useratdomaincom")}.
        to raise_error(ActiveRecord::RecordInvalid)
      expect { @user_invite.update!(email: "user@domaincom")}.
        to raise_error(ActiveRecord::RecordInvalid)
    end

    it "updates status_updated_at field" do
      @user_invite.update!(status: UserInvite::STATE_ACCEPTED)
      expect(@user_invite.reload.status_updated_at).not_to be(nil)
    end
  end
end
