require 'rails_helper'

RSpec.describe UserSetting, type: :model do
  describe "Assocations" do
    it { should belong_to(:user) }
  end
end
