require 'rails_helper'

RSpec.describe User, type: :model do
  describe "Assocations" do
    it { should have_one(:household_user) }
    it { should have_one(:household) }
    it { should have_one(:user_setting) }

    it { should have_many(:financial_account_users) }
    it { should have_many(:financial_accounts) }
    it { should have_many(:user_invites) }
  end
end
