require "rails_helper"

RSpec.describe BalancesController, type: :routing do
  describe "routing" do

    it "routes to #show" do
      expect(:get => "/financial_accounts/1/balances/1").to route_to(
        "balances#show",
        id: "1",
        financial_account_id: "1"
      )
    end

    it "routes to #edit" do
      expect(:get => "/financial_accounts/1/balances/1/edit").to route_to(
        "balances#edit",
        id: "1",
        financial_account_id: "1"
      )
    end

    it "routes to #create" do
      expect(:post => "/financial_accounts/1/balances").to route_to(
        "balances#create",
        financial_account_id: "1"
      )
    end

    it "routes to #update via PUT" do
      expect(:put => "/financial_accounts/1/balances/1").to route_to(
        "balances#update",
        id: "1",
        financial_account_id: "1"
      )
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/financial_accounts/1/balances/1").to route_to(
        "balances#update",
        id: "1",
        financial_account_id: "1"
      )
    end

    it "routes to #destroy" do
      expect(:delete => "/financial_accounts/1/balances/1").to route_to(
        "balances#destroy",
        id: "1",
        financial_account_id: "1"
      )
    end

  end
end
