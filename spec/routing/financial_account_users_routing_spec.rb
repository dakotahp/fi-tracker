require "rails_helper"

RSpec.describe FinancialAccountUsersController, type: :routing do
  describe "routing" do

    it "routes to #create" do
      expect(:post => "/financial_accounts/1/financial_account_users").to route_to(
        "financial_account_users#create",
        financial_account_id: "1"
      )
    end

    it "routes to #destroy" do
      expect(:delete => "/financial_accounts/1/financial_account_users/1").to route_to(
        "financial_account_users#destroy",
        id: "1",
        financial_account_id: "1"
      )
    end

  end
end
