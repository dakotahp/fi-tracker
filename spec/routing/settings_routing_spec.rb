require "rails_helper"

RSpec.describe SettingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/settings").to route_to("settings#index")
    end

  end
end
