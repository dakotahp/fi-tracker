require "rails_helper"

RSpec.describe UserInvitesController, type: :routing do
  describe "routing" do

    it "routes to #create" do
      expect(:post => "/user_invites").to route_to("user_invites#create")
    end

    it "routes to #destroy" do
      expect(:delete => "/user_invites/1").to route_to("user_invites#destroy", id: "1")
    end
  end
end
