require "rails_helper"

RSpec.describe UserSettingsController, type: :routing do
  describe "routing" do

    it "routes to #update via PUT" do
      expect(:put => "/user_settings/1").to route_to("user_settings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/user_settings/1").to route_to("user_settings#update", :id => "1")
    end

  end
end
