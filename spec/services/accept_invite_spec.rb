require 'rails_helper'

RSpec.describe AcceptInvite, type: :service do
  include UsersHelper

  it "accepts an invite and adds to household" do
    user = create_user_with_household
    household = user.household
    invite = create(:user_invite, email: "lloydchristmas@igotworms.com")
    invite.update(user: user, household: household)

    user = create(:user, email: "lloydchristmas@igotworms.com")

    AcceptInvite.new(user, invite.uuid).run

    expect(invite.reload.status).to eq(UserInvite::STATE_ACCEPTED.to_s)
    expect(household.reload.users.size).to eq(2)
  end
end
