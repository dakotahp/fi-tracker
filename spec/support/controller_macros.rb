module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      household = create(:household, :with_user)
      sign_in household.users.first
    end
  end
end
