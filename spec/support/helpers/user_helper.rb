module UsersHelper
  def create_user_with_household
    household = create(:household)
    user = create(:user, email: "esnowden#{Random.rand(100)}@lavabit.com")

    # Link user to household
    household_user = create(
      :household_user,
      household: household,
      user: user
    )

    # Add goal to household
    create(:goal, household: household)

    # Create setings for user
    create(:user_setting, user: user)

    user.reload
  end
end
