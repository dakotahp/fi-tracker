var path = require("path")
var StatsPlugin = require("stats-webpack-plugin")
var webpack = require("webpack")

module.exports = {
  devServer: {
    port: 3808,
    headers: { "Access-Control-Allow-Origin": "*" }
  },
  devtool: "cheap-module-source-map",
  entry: {
    "react": "./app/assets/javascripts/react_bundle"
  },
  output: {
    path: path.join(__dirname, "public/webpack"),
    filename: "[name].js",
    publicPath: "/webpack/"
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      "process.env": {
        "NODE_ENV": JSON.stringify("development")
      }
    }),
    new StatsPlugin("manifest.json", {
      chunkModules: false,
      source: false,
      chunks: false,
      modules: false,
      assets: true
    })
  ],
  resolve: {
    extensions: [".js", ".jsx"]
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: "babel-loader",
      exclude: /node_modules/,
      query: {
        "presets": ["react", "es2015", "stage-0", "react-hmre"]
      }
    }]
  }
}
